import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import { CartProvider } from 'react-use-cart'
import Cart from './pages/Cart';
import Home from './pages/Home';
import Login from './pages/Login';
import AppNavbar from './pages/AppNavbar';
import Logout from './pages/Logout';
import Register from './pages/Register';
import { UserProvider } from './UserContext';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';


function App() {

  const [ user, setUser ] = useState({
    //email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  },[user])
  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <CartProvider>
      <Router>
        <AppNavbar/>
        <Switch>
          <Route exact path = '/'>
          <Home />
          </Route>
          <Route exact path = '/cart'>
          <Cart />
          </Route>
          <Route exact path = '/login'>
          <Login />
          </Route>
          <Route exact path = '/logout'>
          <Logout />
          </Route>
          <Route exact path = '/register'>
          <Register />
          </Route>
        </Switch>
      </Router>
    </CartProvider>
    </UserProvider >    
  )
}

export default App;
