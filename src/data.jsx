const data = {
    productData:[
        {
            id: 1,
            img: '../images/olay.jpg',
            title: 'Olay',
            desc: 'It helps and minimizes wrinkles and pores. ',
            price: 400,
        },
        {
            id: 2,
            img: '../images/presspowder.jpg',
            title: 'presspowder',
            desc: 'It prevents the face from getting oily leaving the skin fresh all day.',
            price: 200,
        },
        {
            id: 3,
            img: '../images/blushon.jpg',
            title: 'Blushon',
            desc: 'Give your skin a pinkish color like blushing.',
            price: 150,
        },
        // {
        //     id: 4,
        //     img: '../images/cccushion.jpg',
        //     title: 'CC cushion SPF45',
        //     desc: 'Protects your skin from UVA and UVB rays from the sun and gives glass skin.',
        //     price: 600,
        // },
        // {
        //     id: 5,
        //     img: '../images/hairtreat.jpg',
        //     title: 'Hair treatment',
        //     desc: 'Treats dry hair',
        //     price: 1200,
        // },
        // {
        //     id: 6,
        //     img: '../images/jojobaoil.jpg',
        //     title: 'Jojoba Oil',
        //     desc: 'Makes your hair shiny and healthy',
        //     price: 340,
        // },
    ]
}
	
export default data