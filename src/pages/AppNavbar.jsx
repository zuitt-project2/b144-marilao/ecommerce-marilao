
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Fragment, useState, useContext } from 'react';
import Container from 'react-bootstrap/Container';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {
	

	const {user} = useContext(UserContext);

	return (
		<Navbar bg="info" expand="lg">
			<Container>
				<Navbar.Brand as={Link} to = '/' >ShanStyle</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse class='text-center' id="basic-navbar-nav">
				<Nav className="me-auto">
					<Nav.Link as={NavLink} to='/' exact>Home</Nav.Link>
					<Nav.Link as={NavLink} to='/cart' exact>Cart</Nav.Link>
					{ (user.id !== null) ?
						<Nav.Link as={NavLink} to='/logout' exact>Logout</Nav.Link>
						:
						<Fragment>
							<Nav.Link as={NavLink} to='/login' exact>Login</Nav.Link>
							<Nav.Link as={NavLink} to='/register' exact>Register</Nav.Link>
						</Fragment>
					}
				</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}
