import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(props) {
    // Allows to consume the User context object and it's properties to use for user validation
    const {user, setUser} = useContext(UserContext);
    const history = useHistory();

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);

    function authenticate(e){
        e.preventDefault();

        // Process a fetch request to the corresponding backend API
        /* Syntax:
            fetch('url', {options})
            .then(res => res.json())
            .then(data => {})
        */
        fetch('http://localhost:3000/api/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data.accessToken)

            if(typeof data.accessToken !== "undefined"){
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to ShanStyle!"
                })
                history.push("/")
            }
            else{
                Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "Check your login details and try again."
                })
            }

        })

        

        setEmail('');
        setPassword('');

        console.log(`${email} has been verified! Welcome back!`);
    }  

    const retrieveUserDetails = (token) => {
        fetch('http://localhost:3000/api/users/details', {
            headers: {
                Authorization: `Bearer ${ token }`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {
        // Validation to enable the submit button when all fields are populated
        if(email !== '' && password !== ''){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [email, password])

    return (
        (user.id !== null) ?
            <Redirect to="/" />
        :
        <Container className='bg-secondary m-5 mx-auto'>
            <h1 className='text-center'>Login</h1>
            <Form onSubmit={(e) => authenticate(e)}>
               <Form.Group className="mb-3 text-center "  controlId="userEmail">
                   <Form.Label>Email address</Form.Label>
                   <Form.Control class='m-3'
                       type="email" 
                       placeholder="Enter email" 
                       value={email}
                       onChange={(e) => setEmail(e.target.value)}
                       required
                   />
               </Form.Group>

               <Form.Group className="mb-3 text-center " controlId="password">
                   <Form.Label>Password</Form.Label>
                   <Form.Control class='m-2'
                       type="password" 
                       placeholder="Password"
                       value={password}
                       onChange={(e) => setPassword(e.target.value)}
                       required
                   />
               </Form.Group>

               { isActive ?
               <div className='text-center'>
                   <Button variant="primary" type="submit" id="submitBtn">
                       Sign in
                   </Button>
               </div>
                   
                   :
                <div className='text-center'>
                   <Button variant="primary" type="submit" id="submitBtn">
                       Sign in
                   </Button>
               </div>
               }
                           
            </Form> 
        </Container>
        
    )
}
