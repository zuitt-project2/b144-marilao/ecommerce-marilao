import { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout() {
	// localStorage.clear();

	//Destructuring
	const {unsetUser, setUser} = useContext(UserContext);

	// Clear the localStorage for the user's information
	unsetUser();

	useEffect(() => {
		// Set the user state back to it'ss original value
		setUser({id: null})
	});
	// Redirect back to login
	return(
		<Redirect to='/login'/>
	)
}