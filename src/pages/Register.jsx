import { useState, useEffect, useContext } from 'react';
import { Redirect, useHistory } from 'react-router-dom';
import { Form, Button, Container } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {

    const { user } = useContext(UserContext);

    const history = useHistory();


    // State hooks to store the values of the input fields
    const [firstName, setFname] = useState('');
    const [lastName, setLname] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);
    /*const [checkEmail, setCheckEmail ] = useState(false)*/

    // Check if values are successfully binded
    console.log(firstName);
    console.log(lastName);
    console.log(email);
    console.log(password1);
    console.log(password2);

    // Function to simulate user registration
    function registerUser(e) {
        // Prevents page redirection viaform submission
        e.preventDefault();


        fetch('http://localhost:3000/api/users/checkEmail', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)

                if (data === true) {
                    Swal.fire({
                        title: 'Duplicate Email found!',
                        icon: 'error',
                        text: 'Please provide a different email.'
                    });
                }
                else {
                    fetch('http://localhost:3000/api/users/register', {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify({
                            firstName: firstName,
                            lastName: lastName,
                            email: email,
                            password: password1
                                                   
                        })

                    })
                        .then(res => res.json())
                        .then(data => {
                            console.log(data);

                            if (data === true) {

                                setFname('');
                                setLname('');
                                setEmail('');
                                setPassword1('');
                                setPassword2('');

                                Swal.fire({
                                    title: "Successfully registered",
                                    icon: "success",
                                    text: "You have successfully registered an account."
                                });
                                history.push("/login")

                            }
                            else {
                                Swal.fire({
                                    title: "Something went wrong",
                                    icon: "error",
                                    text: "Please try again."
                                });
                            }
                        })
                }
            })
    }

    useEffect(() => {
        // Validation to enable the submit button when all fields are populated and both passwords match
        if ((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [firstName, lastName, email, password1, password2])

    return (
        (user.id !== null) ?
            <Redirect to='/' />
            :

            <Container>
                <Form className='m-5  col-12 col-lg-12'  onSubmit={(e) => registerUser(e)}>
                <h1 className='text-center'>Register</h1>
                <Form.Group className="mb-3 text-center" controlId="userEmail">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control class='m-2 text-center'
                        type="text"
                        placeholder="Enter first name"
                        value={firstName}
                        onChange={e => setFname(e.target.value)}
                        required
                    />
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control class='m-2 text-center'
                        type="text"
                        placeholder="Enter last name"
                        value={lastName}
                        onChange={e => setLname(e.target.value)}
                        required
                    />

                    <Form.Label>Email address</Form.Label>
                    <Form.Control class='m-2 text-center'
                        type="email"
                        placeholder="Enter email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required
                    />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text><br />

                </Form.Group>

                <Form.Group className="mb-3 text-center" controlId="password1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control class='m-2 text-center'
                        type="password"
                        placeholder="Password"
                        value={password1}
                        onChange={e => setPassword1(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group className="mb-3 text-center" controlId="password2">
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control class='m-2 text-center'
                        type="password"
                        placeholder="Verify Password"
                        value={password2}
                        onChange={e => setPassword2(e.target.value)}
                        required
                    />
                </Form.Group>


                {isActive ?
                    <div className='text-center'>
                        <Button variant="primary" type="submit" id='submitBtn'>
                            Create
                        </Button>
                    </div>

                    :
                    <div className='text-center'>
                        <Button variant="warning" type="submit" id='submitBtn' disabled>
                            Create
                        </Button>
                    </div>

                }
            </Form>
            </Container>
            
    )
}